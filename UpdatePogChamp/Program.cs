﻿using DSharpPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Web;

namespace UpdatePogChamp
{
    class Program
    {
        private class TwitchAuthResponse
        {
            [JsonPropertyName("access_token")]
            public string AccessToken { get; set; }
        }

        private class TwitchEmoteSets
        {
            [JsonPropertyName("emoticon_sets")]
            public TwitchEmoteSet EmoticonSets { get; set; }
        }

        private class TwitchEmoteSet
        {
            [JsonPropertyName("0")]
            public List<TwitchEmote> Emotes { get; set; }
        }

        private class TwitchEmote
        {
            [JsonPropertyName("code")]
            public string Code { get; set; }

            [JsonPropertyName("id")]
            public int Id { get; set; }
        }

        private class RedditResponse
        {
            public RedditData1 data { get; set; }
        }

        private class RedditData1
        {
            public List<Children> children { get; set; }
        }

        private class Children
        {
            public Data data { get; set; }
        }

        private class Data
        {
            public string permalink { get; set; }
        }

        static void Main(string[] args)
        {
            MainAsync().GetAwaiter().GetResult();
        }

        static async Task MainAsync()
        {
            using var httpClient = new HttpClient();

            var clientId = "";
            var clientSecret = "";

            var builder = new UriBuilder("https://id.twitch.tv/oauth2/token");

            var query = HttpUtility.ParseQueryString(builder.Query);
            query.Add("client_id", clientId);
            query.Add("client_Secret", clientSecret);
            query.Add("grant_type", "client_credentials");

            builder.Query = query.ToString();

            var message = new HttpRequestMessage()
            {
                RequestUri = builder.Uri,
                Method = HttpMethod.Post
            };

            var jsonResponse = await (await httpClient.SendAsync(message)).Content.ReadAsStreamAsync();

            var access_token = (await JsonSerializer.DeserializeAsync<TwitchAuthResponse>(jsonResponse)).AccessToken;

            message = new HttpRequestMessage()
            {
                RequestUri = new Uri($"https://api.twitch.tv/v5/chat/emoticon_images?emotesets=0"),
                Method = HttpMethod.Get
            };

            message.Headers.Add("Client-Id", "");
            message.Headers.Add("Authorization", $"Bearer {access_token}");

            jsonResponse = await (await httpClient.SendAsync(message)).Content.ReadAsStreamAsync();

            var emote_response = (await JsonSerializer.DeserializeAsync<TwitchEmoteSets>(jsonResponse));

            var pogChampId = emote_response.EmoticonSets.Emotes.Where(_ => _.Code == "PogChamp").Single().Id;

            message = new HttpRequestMessage()
            {
                RequestUri = new Uri($"https://static-cdn.jtvnw.net/emoticons/v1/{pogChampId}/5.0")
            };

            var imageResponse = await (await httpClient.SendAsync(message)).Content.ReadAsStreamAsync();

            //Create the Discord client
            DiscordClient discord = new DiscordClient(new DiscordConfiguration
            {
                Token = "",
                TokenType = TokenType.Bot,
                LogLevel = LogLevel.Debug
            });

            discord.GuildAvailable += async (eventArgs) =>
            {
                var guild = eventArgs.Guild;

                if (guild.Name == "wat")
                {
                    var bunchOfNerds = guild.Channels.Where(_ => _.Name == "bunchofnerds").Single();

                    try
                    {
                        var emoji = guild.GetEmojisAsync().Result.Where(_ => _.Name == "pog").Single();
                        guild.DeleteEmojiAsync(emoji).Wait();
                    }
                    catch
                    {
                        await bunchOfNerds.SendMessageAsync("Some bum deleted pog. I'll go ahead and fix their mistake.");
                    }
                    finally
                    {
                        guild.CreateEmojiAsync("pog", imageResponse).Wait();

                        var pog = guild.GetEmojisAsync().Result.Where(_ => _.Name == "pog").Single();

                        await bunchOfNerds.SendMessageAsync(pog + "🌭");
                    }
                }
            };

            await discord.ConnectAsync();

            await Task.Delay(10000);
        }
    }
}
